const CracoLessPlugin = require('craco-less')

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            // modifyVars: {
            //   'primary-color': '#FFC706',
            //   'link-color': '#FFC706',
            //   'border-radius-base': '2px',
            // },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
}
