
import App from 'App.js'
import Login from './page/Login/index'
import { createBrowserHistory } from 'history'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

const customHistory = createBrowserHistory()

const Root = () => {
  return (
    <Router history={customHistory}>
      <Switch>
        {/* <Route component={Login} /> */}
        <Route component={App} />
      </Switch>
    </Router>
  )
}
export default Root