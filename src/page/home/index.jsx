import React from "react";
import styled from "styled-components";
import { Carousel } from 'antd';
export default function index() {
  const contentStyle = {
    height: '160px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
  };
  return (
    <div>
      <div style={{
        backgroundColor: "#ffff",
        zIndex: "1",
        paddingTop: '20px',
        paddingBottom: '20px',
        display: 'flex',
        justifyContent: 'space-around'
      }}>
        <Container>

          <div class="row">
            <div class="col-sm-4" >
              <div class="card">
                {" "}
                <img
                  src="https://cdn.mwallpapers.com/photos/celebrities/living-room/modern-luxury-indian-living-room-design-decoration-45715hd-wallpapers-desktop-background-android-iphone-1080p-4k-beupe.jpg"
                  style={{ width: '500px', height: '300px' }}
                  alt="Avatar"
                />
                <div class="container">
                  <Text> พาโนเพลส</Text>
                  <Text>คลองหก คลองหลวง ปทุมธานี</Text>
                  <Text>฿3,500 /รายเดือน</Text>
                </div>
              </div>
            </div>
            <div class="col-sm-4" >
              <div class="card">
                {" "}
                <img
                  src="https://i.ebayimg.com/images/g/OUQAAOSwf9teHzMk/s-l500.jpg"
                  style={{ width: '500px', height: '300px' }}
                  alt="Avatar"
                />
                <div class="container">
                  <Text> พาโนเพลส</Text>
                  <Text>คลองหก คลองหลวง ปทุมธานี</Text>
                  <Text>฿3,500 /รายเดือน</Text>
                </div>
              </div>
            </div>
            <div class="col-sm-4" >
              <div class="card">
                {" "}
                <img
                  src="https://i.ebayimg.com/images/g/OUQAAOSwf9teHzMk/s-l500.jpg"
                  style={{ width: '500px', height: '300px' }}
                  alt="Avatar"
                />
                <div class="container">
                  <Text> พาโนเพลส</Text>
                  <Text>คลองหก คลองหลวง ปทุมธานี</Text>
                  <Text>฿3,500 /รายเดือน</Text>
                </div>
              </div>
            </div>
          </div>

        </Container>
      </div>
    </div>
  );
}

const Container = styled.div`
  .card {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
  }

  .card:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  }

  .container {
    padding: 2px 16px;
  }
`;

const Text = styled.p`
  color: black;
  font-size: 20px;
`;
