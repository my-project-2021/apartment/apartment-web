import { Layout } from 'antd';
import Header from './component/Header/index'
import Footer from './component/Footer/index'
import ContextApp from './component/routes'
import Home from './page/home/'
import "./App.css";

const { Content } = Layout;

function App(props) {
  return (
    <>

      <Layout style={{ minHeight: '100vh' }}>
        <Header {...props} />
        <Layout className='site-layout'>
          <Home />
          <Content >
            <ContextApp></ContextApp>
          </Content>
        </Layout>
        <Footer />
      </Layout>
    </>
  );
}

export default App;
