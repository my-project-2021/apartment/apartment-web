import React from 'react'
import Home from '../../page/home/index'
import Contract from '../../page/contract/index'
import { Route, Switch } from 'react-router-dom'

export default function Routes() {
  return (
    <>
      <Switch>
        <Route exact component={Home} path='/'> </Route>
        <Route exact component={Home} path='/home'></Route>
        <Route exact component={Contract} path='/Contract'></Route>
      </Switch>
    </>
  )
}
