import React from 'react'
import ParticlesBg from "particles-bg";
import Fade from "react-reveal";
export default function index() {
  return (
    <div>
      <header id="home">
        <div class="container" style={{ width: '100%', height: '800px' }}>
          <h2>Carousel Example</h2>
          <div id="myCarousel" class="carousel slide" data-ride="carousel" >
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" style={{ width: '100%', height: '700px' }} >
              <div class="item active" >
                <img src="https://wallpaper.dog/large/10981541.jpg" alt="Los Angeles" style={{ width: '100%', height: '700px' }} />
              </div>
              <div class="item">
                <img src="https://wp-assets.dotproperty-kh.com/wp-content/uploads/sites/2/2017/03/13154906/Fotolia_101262853_Subscription_Monthly_M.jpg" alt="Chicago" style={{ width: '100%', height: '700px' }} />
              </div>
              <div class="item">
                <img src="https://www.technocrazed.com/wp-content/uploads/2015/12/Home-Wallpaper-9.jpg" alt="New york" style={{ width: '100%', height: '700px' }} />
              </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>

        <nav id="nav-wrap">
          <a className="mobile-btn" href="#nav-wrap" title="Show navigation">
            Show navigation
          </a>
          <a className="mobile-btn" href="#home" title="Hide navigation">
            Hide navigation
          </a>
          <ul id="nav" className="nav" >
            <li className="current">
              <a className="smoothscroll" href="#home" style={{ color: 'black' }}>
                Home
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#about" style={{ color: 'black' }}>
                About
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#resume" style={{ color: 'black' }}>
                Resume
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#portfolio" style={{ color: 'black' }}>
                Works
              </a>
            </li>
            <li>
              <a className="smoothscroll" href="#contact" style={{ color: 'black' }}>
                Contact
              </a>
            </li>
          </ul>
        </nav>
      </header>

    </div>
  )
}
